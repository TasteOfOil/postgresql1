const express = require('express');
const cors = require('cors');
const controller = require('./controller')
const app = express();

const port = process.env.PORT || 3000;
app.listen(port, ()=>{
   console.log(`listening on port:${port}`);
});

app.use(express.json());
app.use(cors({
   origin:'*'
}));

app.all(()=>{
   console.log(req.url);
})

app.get('/', (req,res)=>{
   console.log("Start page");
   res.status(200).send("It's start page");
});

app.get('/api/students', controller.getStudents);
app.get('/api/students/:id', controller.getStudent);
app.post('/api/students/', controller.createStudent);
app.put('/api/students/:id',controller.updateStudent);
app.delete('/api/students/:id',controller.deleteStudent);