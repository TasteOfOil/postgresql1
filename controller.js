const pool = require('./connectionDB');




const createStudent = async (req,res)=>{
    try{
        await pool.query('insert into students (name,age) values ($1,$2)', [req.body.name, req.body.age]);
        console.log("POST student");
        res.status(200).send("Student has been successfully added");
    }
    catch(err){
        console.log(err);
        res.status(500).send(`Error 500`);
    }
}
const getStudent = async (req,res)=>{
    try{
        const id = req.params.id;
        const result = await pool.query('select * from students where id = $1', [id]);
        if(result.rows.length==0){
            throw new Error("Student not found");
        }
        console.log("GET student");
        res.status(200).send(result.rows);
    }
    catch(err){
        console.log(err);
        res.status(500).send(`Error 500`);
    }
}
const getStudents = async (req,res)=>{
    try{
        const result = await pool.query('select * from students');
        console.log("GET students");
        res.status(200).send(result.rows);
    }
    catch(err){
        console.log(err);
        res.status(500).send(`Error 500`);
    }
}
const updateStudent = async (req,res)=>{
    try{
        const id = req.params.id;
        const result = await pool.query("select * from students where id = $1", [id]);
        
        if(result.rows.length==0){
            throw new Error("Student not found");
            
        }
        const student = {
            name: req.body.name,
            age: req.body.age 
        };
        if(!student.name && !student.age){
            throw new Error("Data entered incorrectly");
        }
        else if(!student.name){
            await pool.query("update students set age = $1 where id = $2", [student.age, id ]);
        }
        else if(!student.age){
            await pool.query("update students set name = $1 where id = $2", [ student.name, id]);   
        }
        else{
            await pool.query("update students set name = $1, age = $2 where id = $3", [student.name, student.age, id]);
        }
        await res.status(200).send("Data updated successfully");
        console.log("PUT student");
    }
    catch(err){
        console.log(err);
        res.status(500).send(`Error 500`);
    }
}
const deleteStudent = async (req,res)=>{
    try{
        const id = req.params.id;
        const result = await pool.query("select * from students where id = $1", [id]);
        
        if(result.rows.length==0){
            throw new Error("Student not found");
            
        }
        await pool.query("delete from students where id = $1", [id]);
        await res.status(200).send("Data deleted successfully");
        console.log("DELETE student");
    }
    catch(err){
        console.log(err);
        res.status(500).send(`Error 500`);
    }
}

module.exports = {createStudent, getStudent,getStudents,
                updateStudent, deleteStudent};
